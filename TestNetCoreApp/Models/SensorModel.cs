﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace TestNetCoreApp.Models
{
    public class SensorModel
    {
        public int ID { get; set; }
        public string Description { get; set; }

        [Required(ErrorMessage = "Min value is required")]
        [Min(nameof(MaxValue))]
        public double MinValue { get; set; }

        [Required(ErrorMessage = "Max value is required")]
        [Remote(action: "VerifyMaxValue", controller: "Test", AdditionalFields = nameof(MinValue))]
        public double MaxValue { get; set; }

        [Required]
        [Country]
        public string Country { get; set; }
    }

    public class CountryAttribute : ValidationAttribute, IClientModelValidator
    {
        public void AddValidation(ClientModelValidationContext context)
        {
            context.Attributes.Add("data-val", "true");
            context.Attributes.Add("data-val-country", "Invalid country. Valid values are USA, UK, and India---.");
        }

        protected override ValidationResult IsValid (object value, ValidationContext validationContext)
        {
            string country = value.ToString();

            if (country != "USA" && country != "UK" && country != "India")
            {
                return new ValidationResult("Invalid country. Valid values are USA, UK, and India+++.");
            }
            return ValidationResult.Success;
        }
    }

    public class MinAttribute : ValidationAttribute, IClientModelValidator
    {
        private string _comparedValue;

        public MinAttribute(string compareValue)
        {
            _comparedValue = compareValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (SensorModel)validationContext.ObjectInstance;
            var maxVal = model.MaxValue;
            var minVal = (double)value;
            if (minVal >= maxVal)
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }

        public string ComparedValue => _comparedValue;

        public string GetErrorMessage()
        {
            return $"Min value should be less than max value";
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            context.Attributes.Add("data-val", "true");
            context.Attributes.Add("data-val-min", GetErrorMessage());
        }
    }

    //public class CompareValueAttributeAdapter : AttributeAdapterBase<CompareValueAttribute>
    //{
    //    private string _comparedValue;

    //    public CompareValueAttributeAdapter(CompareValueAttribute attribute, IStringLocalizer stringLocalizer) : base(attribute, stringLocalizer)
    //    {
    //        _comparedValue = attribute.ComparedValue;
    //    }
    //    public override void AddValidation(ClientModelValidationContext context)
    //    {
    //        if (context == null)
    //        {
    //            throw new ArgumentNullException(nameof(context));
    //        }

    //        MergeAttribute(context.Attributes, "data-val", "true");
    //        MergeAttribute(context.Attributes, "data-val-comapare", GetErrorMessage(context));

    //        var comVal = Attribute.ComparedValue.ToString(CultureInfo.InvariantCulture);
    //        MergeAttribute(context.Attributes, "data-val-comapare-val", comVal);
    //    }
    //    public override string GetErrorMessage(ModelValidationContextBase validationContext)
    //    {
    //        return Attribute.GetErrorMessage();
    //    }
    //}

    //public class CustomValidationAttributeAdapterProvider :
    //IValidationAttributeAdapterProvider
    //{
    //    IValidationAttributeAdapterProvider baseProvider =
    //        new ValidationAttributeAdapterProvider();
    //    public IAttributeAdapter GetAttributeAdapter(ValidationAttribute attribute,
    //        IStringLocalizer stringLocalizer)
    //    {
    //        if (attribute is CompareValueAttribute)
    //        {
    //            return new CompareValueAttributeAdapter(
    //                attribute as CompareValueAttribute, stringLocalizer);
    //        }
    //        else
    //        {
    //            return baseProvider.GetAttributeAdapter(attribute, stringLocalizer);
    //        }
    //    }
    //}
}
