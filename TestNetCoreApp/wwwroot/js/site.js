﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

//$.validator.addMethod('comparevalue',
//    function (value, element, params) {
//        // Get element value. Classic genre has value '0'.
//        var genre = $(params[0]).val(),
//            year = params[1],
//            date = new Date(value);
//        if (genre && genre.length > 0 && genre[0] === '0') {
//            // Since this is a classic movie, invalid if release date is after given year.
//            return date.getUTCFullYear() <= year;
//        }

//        return true;
//    });

//$.validator.unobtrusive.adapters.add('comparevalue',
//    ['year'],
//    function (options) {
//        var element = $(options.form).find('select#Genre')[0];
//        options.rules['comparevalue'] = [element, parseInt(options.params['year'])];
//        options.messages['comparevalue'] = options.message;
//    });

$(function () {
    jQuery.validator.addMethod("country",
        function (value, element, param) {
            //console.log(value);
            //console.log(element);
            //console.log(param);
            if (value !== "USA" && value !== "UK" && value !== "India") {
                return false;
            }
            else {
                return true;
            }
        });

    jQuery.validator.unobtrusive.adapters.addBool("country");
});