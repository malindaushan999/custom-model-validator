﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestNetCoreApp.Models;

namespace TestNetCoreApp.Controllers
{
    public class TestController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create(SensorModel model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Message = "Model is Valid";
            }
            else
            {
                ViewBag.Message = "Model is Invalid";
            }
            return View("Index");
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult VerifyMaxValue(double maxValue, double minValue)
        {
            if (maxValue <= minValue)
            {
                return Json(data: $"Max value should be greater than min value");
            }
            return Json(data: true);
        }
    }
}